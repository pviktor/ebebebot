FROM python:3.7-slim

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY bot.py /bin

CMD python3 /bin/bot.py
