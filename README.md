# ebebebot

## Run
```shell
python3 -m venv env
. venv/bin/activate
pip3 install -r requirements.txt
CYKA_TOKEN='your_bot_token' python3 bot.py
```

Run in docker:
```shell
docker build -t ebebebot .
docker run -d -e CYKA_TOKEN='your_token' ebebebot
```