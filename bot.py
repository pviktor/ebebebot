from telegram import *
from telegram.ext import *
from functools import reduce, lru_cache
import html
import os

TOKEN = os.getenv('CYKA_TOKEN')


def rokk_ebol_command(u, c):
    out = rokk_ebol(u.message.text)
    try:
        u.message.reply_text(text=out, parse_mode=ParseMode.HTML)
    except Exception as ex:
        print(f'Cannot send: {ex}')


def rokk_ebol_inline(u, c):
    tquery = u.inline_query.query
    umf = u.inline_query.from_user

    print(tquery)

    try:
        rokk_ebol_res = rokk_ebol('/rokk_ebol ' + tquery)
        results = [
            InlineQueryResultArticle(
                id=umf.id,
                title="рокк ебол",
                input_message_content=InputTextMessageContent(rokk_ebol_res, parse_mode=ParseMode.HTML),
            description='рокк ебол')
        ]
        u.inline_query.answer(results)
    except Exception as ex:
        print(f'Cannot send: {ex}')


@lru_cache(maxsize=1024)
def rokk_ebol(text):
    try:
        command = str(text).split()
        command = command[1:]
        if len(command) < 1:
            raise ValueError

        output = ''
        for i in range(max([len(x) for x in command])):
            for c in command:
                try:
                    output += c[i] + ' '
                except IndexError:
                    output += '  '
            output += '\n'

        output = output.strip()

        return f'<code>{html.escape(output)}</code>'

    except Exception as ex:
        return f'Unable to parse. Usage: <code>/rokk_ebol STR [STR ...]</code>'


def histogram(update, context):
    mink = 0
    maxk = 100
    try:
        command = str(update.message.text).split()
        command = command[1:]
        if len(command) < 1:
            raise ValueError

        for i in command:
            if 'max=' in i:
                maxk = int(i[4:])
            #if 'min=' in i:
                #mink = int(i[4:])

        command = [x for x in command if 'max=' not in x]
        command = [int(x) for x in command]
    except:
        try:
            update.message.reply_text(
                text=f'Unable to parse. Usage: <code>/histogram INT [INT ...] [max=INT]</code>', parse_mode=ParseMode.HTML)
        except Exception as e:
            print(f'Cannot send: {e}')
        return

    try:
        h = Histogram(command, mink, maxk)
        try:
            update.message.reply_text(
                text=f'<code>{h.pretty_string()}</code>', parse_mode=ParseMode.HTML)
        except Exception as e:
            print(f'Cannot send: {e}')
    except Exception as e:
        try:
            update.message.reply_text(
                text=f'Error during command execution', parse_mode=ParseMode.HTML)
        except Exception as e:
            print(f'Cannot send: {e}')
        print(f'Error during command execution: {e}')


class Histogram:
    def __init__(self, values, mink=0, maxk=100):
        if mink >= maxk:
            raise ValueError

        self.data = {}

        for x in range(mink, maxk // 10 + 1):
            self.data.setdefault(x * 10, 0)

        for value in values:
            if value > maxk:
                value = maxk
            key = value // 10
            self.data[key * 10] += 1

    def pretty_string(self, maxlen=15):
        # normalize
        m = max(self.data.values())
        if m > maxlen:
            coeff = m / maxlen
            data = {k: round(v / coeff) for k, v in self.data.items()}
        else:
            data = self.data

        def gen_stars(count):
            return '*' * count

        arr = sorted(data.items(), key=lambda x: x[0])
        output = reduce(
            lambda a, x: f'{a}{x[0]:3d} - {x[0]+9:3d}: {gen_stars(x[1])}\n',
            arr[:-1], '')
        output += f'{arr[-1][0]:3d}+     : {gen_stars(arr[-1][1])}'
        return output


def help(u, c):
    text = f'''
<b>Commands</b>
<code>/histogram INT [INT ...] [max=INT]</code> – create histogram from given integer values
<code>/rokk_ebol STR [STR ...]</code> – print text vertically

<b>Inline</b>
<code>@ebebebot STR [STR ...]</code> – print text vertically

Chat: @tovbotn
'''.strip()

    try:
        u.message.reply_text(text=text, parse_mode=ParseMode.HTML)
    except Exception as ex:
        print(f'Cannot send: {ex}')



def main():
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('rokk_ebol', rokk_ebol_command))
    dp.add_handler(CommandHandler('histogram', histogram))

    dp.add_handler(CommandHandler('start', help))
    dp.add_handler(CommandHandler('help', help))

    dp.add_handler(InlineQueryHandler(rokk_ebol_inline))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()

